<!DOCTYPE html>
<html>

<head>
    <link rel='icon' href='P.ico' type='image/x-icon' />
    <meta charset="UTF-8">
    <link rel="stylesheet" href="styleSheet.css" type="text/css" />
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
    <script type="text/javascript" src="scripts\jScript.js"></script>
    <title>Porosit Online</title>
</head>

<body onload="nextSlide(0)">
    <div class="main-div">
        <div class="header">
            <div id="logo">
                <a href="home.php">
                    <img src="Logo.png" id="LogoImg" />
                </a>
            </div>
            <div class="search-box">

                <input type="text" name="" class="search-txt" placeholder="Type to search" />
                <a class="search-btn" name="search-btn" href="#">
                    <i class="fas fa-search"></i>
                </a>
            </div>
            <?php 
                require("functions.php");
                if (isLoggedIn()) {
                    if(isAdmin()){
                        echo '<a href="admin/home.php" class="AccLink">Account</a>';
                    }else{
                        echo '<a href="index.php" class="AccLink">Account</a>';
                    }
                }else{
                    echo '<h6 class="Account login"><a href="login.php" class="AccLink">Log In</a></h6>';
                }
                
                echo '<style type="text/css">
                    .AccLink{
                        position: absolute;
                        top:4%;
                        left: 84%;
                        text-decoration: none;
                        font-family: Roboto, sans-serif;
                        color: white;
                    }
                    .AccLink:hover{
                        text-decoration: underline;
                    }
                    </style>'
            ?>
            <div class="lines">
                <div class="shopping-cart">
                    <a class="shopping-btn" href="shopping-cart/index.php">
                        <i class="fas fa-shopping-cart"></i>
                    </a>
                </div>
            </div>
            <div class="menu">
                <table class="menu-table">
                    <tr>
                        <td class="tdLink">
                            <a href="kozmetik.php" class="Link">
                                Kozmetike
                            </a>
                        </td>
                        <td class="tdLink">
                            <a href="kepuce.php" class="Link">
                                Kepuce
                            </a>
                        </td>
                        <td class="tdLink">
                            <a href="bluza.php" class="Link">
                                Bluza
                            </a>
                        </td>
                        <td class="tdLink">
                            <a href="aksesore.php" class="Link">
                                Aksesore
                            </a>
                        </td>
                        <td class="tdLink">
                            <a href="parfume.php" class="Link">
                                Parfume
                            </a>
                        </td>
                        <td class="tdLink">
                            <a href="shtiklla.php" class="Link">
                                Shtiklla
                            </a>
                        </td>
                        <td class="tdLink" id="last">
                            <a href="fustana.php" class="Link">
                                Fustana
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
        
        <div class="containers">

            <?php
                $connect = mysqli_connect("localhost", "root", "", "porosionlinedb");  
                
                             
                $counter = 0;
                $query = "SELECT * FROM tbl_images where product_name LIKE '%aksesor%' ORDER BY id DESC"; 
                $result = mysqli_query($connect, $query);
                while($row = mysqli_fetch_array($result))
                {
                    echo '
                        <form method="post" action="AddToCart.php">
                            <div class="container">
                                <div class="Ad-Image">
                                    <a href="#" class="Ad-Link">
                                        <img src="data:image/jpeg;base64,'.base64_encode($row['name'] ).'" class="imgSrc cImg'.$counter.'" />
                                    </a>
                                </div>
                                <input type="hidden" name="hiddenID'.$counter.'" value="'.($row['id']).'"/>
                                <div class="emertimi">'.($row['product_name']).'</div>
                                <span class="price">$'.($row['price']).'</span>
                                <div class="add-article">
                                    <div class="link-article">
                                        <input type="submit" name="addToCart'.$counter++.'" value="Add To Cart" />
                                    </div>
                                </div>
                            </div>
                        </form>
                     ';
                }      
            ?>

        </div>
    </div>
    <div class="footer">
        <div class="component">
            <ul>
                <li class="head"> Kontakti </li>
                <li>Kosove: +383 45 111 111 </li>
                <li>Shqiperi: +355 68 111 1111</li>
                <li>contact@porosionline.net</li>
                <li><span>Ahmet Krasniqi, Veranda C2.7 </span><span>Hyrja II, Lokali 7,</span><span>Prishtinë, Kosovë</span></li>
            </ul>
        </div>
        <div class="component">
            <ul>
                <li class="head"> Lloagria</li>
                <li><a href="login.php">Kyqu</a></li>
                <li><a href="register.php">Regjistrohu</a></li>
                <li><a href="contact.html">Kontakti</a></li>
                <li></li>
            </ul>
        </div>
        <div id="footer-logo">
            <img src="Logo.png" id="LogoImg" />
            <div id="copyrights">
                <p>Fuqizuar nga PorosiOnline, Inc. - Të gjitha të drejtat e rezervuara</p>
            </div>
        </div>
    </div>
</body>

</html>
