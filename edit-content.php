 <?php  
 $connect = mysqli_connect("localhost", "root", "", "porosionlinedb");  
    if(isset($_POST["insert"]))  
        {  
            $file = addslashes(file_get_contents($_FILES["image"]["tmp_name"]));  
            $product_name = $_POST["product_name"];  
            $price = $_POST["price"];
            $quantity = $_POST["quantity"];
            $slider = $_POST["slider"];
            $query = "INSERT INTO tbl_images(name,product_name,price,quantity,Slider) VALUES ('$file','$product_name','$price',$quantity,'$slider')";
            if(mysqli_query($connect, $query)){  
                echo '<script>alert("Product Inserted into Database")</script>'; 
            }
        }   
    if(isset($_POST["delete"])){
        $id = $_POST["hiddenID"];
        $deleteQuery = "DELETE FROM tbl_images Where id='$id'";
        
        mysqli_query($connect, $deleteQuery);
        
    }
 ?>
 <!DOCTYPE html>
 <html>

 <head>
     <title>Edit Content</title>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 </head>

 <body>
     <br /><br />
     <div class="container" style="width:500px;">
         <h3 align="center">Edit Content</h3>
         <br />
         <form method="post" enctype="multipart/form-data">
             <input type="file" name="image" id="image" /><br />
             <label for="product_name">Product Name:</label><br />
             <input type="text" name="product_name" id="product_name" /><br />
             <label for="price">Price:</label><br />
             <input type="text" name="price" id="price" /><br />
             <label for="quantity">Quantity:</label><br />
             <input type="number" name="quantity" id="quantity" min="1" value="1"/><br />
             <label for="quantity">Slider(1 for true, 0 for false):</label><br />
             <input type="text" name="slider" id="slider" /><br />
             <br />
             <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-info" />
         </form>
         <br />
         <br />
         <table class="table table-bordered">
             <tr>
                 <th colspan="2">Product</th>
             </tr>
             <?php
                $query = "SELECT * FROM tbl_images ORDER BY id DESC";  
                $result = mysqli_query($connect, $query);  
                while($row = mysqli_fetch_array($result))  
                {  
                    if($row['Slider'] == 0){
                        echo '
                              <tr>  
                                   <td>  
                                        <img src="data:image/jpeg;base64,'.base64_encode($row['name']).'" height="200" width="200" class="img-thumnail" />
                                   </td>  
                                   <td>
                                   <h4>Quantity: '.($row['quantity'] ).'</h4>
                                   <div class="product_name"><h4>Product Name: '.($row['product_name']).'</h4></div>
                                        <form method="post">
                                        <input type="hidden" name="hiddenID" value="'.($row['id'] ).'"/>
                                        <input type="submit" name="delete" value="Delete" />
                                        </form>
                                    </td>
                              </tr>  
                         ';
                    }elseif($row['Slider'] == 1){
                        echo '
                              <tr>  
                                   <td>  
                                        <img src="data:image/jpeg;base64,'.base64_encode($row['name']).'" height="200" width="200" class="img-thumnail" />
                                   </td>
                                   <td>
                                   <h4 style="color:red; text-decoration:underline;">For Slider</h4>
                                   <h4>Quantity: '.($row['quantity'] ).'</h4>
                                   <div class="product_name"><h4>Product Name: '.($row['product_name']).'</h4></div>
                                        <form method="post">
                                        <input type="hidden" name="hiddenID" value="'.($row['id'] ).'"/>
                                        <input type="submit" name="delete" value="Delete" />
                                        </form>
                                    </td>
                              </tr> 
                         ';
                    }
                }  
                ?>
         </table>
     </div>
 </body>

 </html>
 <script>
     $(document).ready(function() {
         $('#insert').click(function() {
             $check = 1;
             var image_name = $('#image').val();
             if (image_name == '') {
                 alert("Please Select Image");
                 return false;
             } else {
                 var extension = $('#image').val().split('.').pop().toLowerCase();
                 if (jQuery.inArray(extension, ['png', 'jpg', 'jpeg']) == -1) {
                     alert('Invalid Image File');
                     $('#image').val('');
                     return false;
                 }
             }
             let regex = new RegExp(/[^0-9]./, 'g');
             let x = document.forms["myForm"]["price"].value;
             let x1 = document.forms["myForm"]["quantity"].value;
             let x1 = document.forms["myForm"]["slider"].value;
             
             if (x.match(regex) && x1.match(regex)) {
                 alert("Must be a valid number");
                 return false;
             }
         });
     });

 </script>
